<?php

namespace Jackophyte;

use Illuminate\Support\ServiceProvider;
use Jackophyte\Jackophyte;

class JackophyteServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(Jackophyte::class, function(){
          return new Jackophyte();
        });
    }
}
